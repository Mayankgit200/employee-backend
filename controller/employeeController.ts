import { Request,RequestHandler,Response } from "express";
import * as employeeService from "../services/employeeServices";
import sendMail from "../src/nodeMailer";

interface EmployeeDocument {
  employeename:string;
  image:string;
  email:string;
  contactnumber:number;
  designation:String;
  joiningdate:Date;
  leaves:Number;
}


export const AddEmployee:RequestHandler= async(req:Request,res:Response)=>{
    const data: EmployeeDocument= req.body;
    await employeeService.addEmployee(data)
    .then((data)=>{
      res.json(data)
    })
    .catch(err=>{
      res.status(400).send(err.message)
    });
}

export const findEmployee: RequestHandler= async(req:Request, res:Response)=>{
    let id: string = req.params.id;
    employeeService.findEmployee({_id:id})
      .then((data)=>{
        //console.log(data)
        res.json(data);
      })
      .catch(err=>{
        res.status(400).send(err.message)
      });
  }


  export const deleteEmployee: RequestHandler= async(req:Request, res:Response)=>{
    let id = req.params.id;
    employeeService.deleteEmployee({_id:id})
      .then((data)=>{
        res.json(data);
      })
      .catch(err=>{
        res.status(400).send(err.message)
      });
  }
   

  
  export const updateEmployee: RequestHandler= async(req:Request, res:Response)=>{
    let id = req.params.id;
    const {employeename,email,contactnumber,designation,joiningdate,leaves}=req.body;
    const record=await employeeService.UpdateDeatils({_id:id},{employeename:employeename, email:email, contactnumber:contactnumber, designation:designation, joiningdate:joiningdate, leaves:leaves},{new:true});
    if(record){
        if(record.leaves===2){
          await sendMail({
            from: process.env.mail,
            mail: record.email,
            subject: `Leaves`,
            message: `got it`,
          });
          res.status(200).json({
            success: true,
            message: `Email sent to ${record.email} successfully`,
          });
        }
      }
    }