import mongoose, {Schema, model, Document} from "mongoose";

export interface EmployeeDocument extends Document{
    employeename:string;
    image:string;
    email:string;
    contactnumber:number;
    designation:String;
    joiningdate:Date;
    leaves:Number;
}

const employeeSchema= new Schema<EmployeeDocument>({
    employeename:{
        type:String,
        required:true
    },
    image:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    contactnumber:{
        type:Number,
        required:true
    },
    designation:{
        type:String,
        required:true
    },
    joiningdate:{
        type:Date,
        required:true
    },
    leaves:{
        type:Number,
        required:true
    },
})

const employeeModel= mongoose.model<EmployeeDocument>("Employee",employeeSchema);
export default employeeModel;