import {DocumentDefinition, FilterQuery, UpdateQuery,QueryOptions} from "mongoose";
import employee,{EmployeeDocument} from "../models/employee"


export function addEmployee(input:DocumentDefinition<EmployeeDocument>){
    return employee.create(input)
}

export function findEmployee(query:FilterQuery<EmployeeDocument>,options:QueryOptions={lean:true}){
    return employee.find(query,{},options);
}

export function UpdateDeatils( 
    query:FilterQuery<EmployeeDocument>,
    update:UpdateQuery<EmployeeDocument>,
    options:QueryOptions
){
    return employee.findByIdAndUpdate(query, update, options)
}

export function deleteEmployee(
    query:FilterQuery<EmployeeDocument>
){
    return employee.deleteOne(query)
}