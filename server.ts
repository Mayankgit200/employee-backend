import express,{Request,Response,NextFunction} from "express";
import router from "./routes/employeeRoutes"
import mongoose,{connect} from "mongoose";
const dotenv= require("dotenv");
dotenv.config({path:"./.env"});


var Employee = mongoose.model('Employee');
const app= express();
app.use('/', router)
mongoose.set('strictQuery', false);


//mongoDB connection
function connects(){
    return connect('mongodb://localhost:27017/employee')
    .then(()=>{
        console.log("db connected")
    }).catch((error:any)=>{
        console.log(error)
    })
}
export default connects;
connects();


//server connected
app.listen(process.env.PORT,():void=>{
    console.log(`server is running on ${process.env.PORT}`)
})