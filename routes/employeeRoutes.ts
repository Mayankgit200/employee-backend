import {Router} from "express";
import {AddEmployee,findEmployee, deleteEmployee, updateEmployee} from "../controller/employeeController"
import bodyParser, { BodyParser } from "body-parser";

const router= Router();
router.use(bodyParser.json())
router.post("/add", AddEmployee)
router.get("/get/:id", findEmployee)
router.delete("/delete/:id",deleteEmployee)
router.put("/update/:id", updateEmployee)
//router.get("/check/:id",check)



export default router;