const nodeMailer = require("nodemailer");

 const sendMail = async (options: any) => {
  const transporter = nodeMailer.createTransport({
   
    service: process.env.Service,
    auth: {
      user: process.env.mail,
      pass: process.env.Password,
    },
  });

  const mailOptions = {
    from: process.env.mail,
    to: options.mail,
    subject: options.subject,
    text: options.message,
  };

  await transporter.sendMail(mailOptions, function(error: any, info:any){
    if(error){
      console.log(error);
    }
    else{
      console.log('Email sent: ' + info.response);
    }
  })
 };

export default sendMail;

// import * as nodeMailer from 'nodemailer';
// import * as SendGrid from 'nodemailer-sendgrid-transport';

// export class NodeMailer {
//     private static initializeTransport() {
//         return nodeMailer.createTransport(SendGrid({
//             auth: {
//                 api_key: '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'
//             }
//         }))
//     }

//     static sendMail(data: { to: [string], subject: string, html: string }): Promise<any> {
//         return NodeMailer.initializeTransport().sendMail({
//             from: 'abc@gmail.com',
//             to: data.to,
//             subject: data.subject,
//             html: data.html
//         });
//     }
// }